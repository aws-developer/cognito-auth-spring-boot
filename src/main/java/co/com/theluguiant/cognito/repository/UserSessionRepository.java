package co.com.theluguiant.cognito.repository;

import co.com.theluguiant.cognito.model.dynamodb.UserSession;
import org.springframework.beans.factory.annotation.Value;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.Optional;

public class UserSessionRepository extends BaseRepository<UserSession>{
    @Value("${amazon.dynamodb.usersession.table}")
    private String table;

    @Value("#{new Boolean('${amazon.dynamodb.usersession.consistentRead}')}")
    private boolean consistentRead;

    @Override
    public String getTableName(){
        return table;
    }

    @Override
    public Boolean getConsistentRead(){
        return consistentRead;
    }

    public UserSession findByClientIDAndUser(String dataPK, Optional<String> SK){
        return getItemByPartition(AttributeValue.builder().s(dataPK).build(), SK);
    }

}
