package co.com.theluguiant.cognito.repository;

import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import org.springframework.beans.factory.annotation.Autowired;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbIndex;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.enhanced.dynamodb.model.GetItemEnhancedRequest;
import software.amazon.awssdk.enhanced.dynamodb.model.Page;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryConditional;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.enhanced.dynamodb.Expression;
import software.amazon.awssdk.services.dynamodb.model.QueryRequest;

import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.stream.Collectors;

public abstract class BaseRepository<I> {

    private DynamoDbEnhancedClient dynamoDbenhancedClient;

    @Autowired
    public void setDynamoDbenhancedClient(DynamoDbEnhancedClient dynamoDbenhancedClient) {
        this.dynamoDbenhancedClient = dynamoDbenhancedClient;
    }

    private String tableName;
    private Boolean consistentRead;

    protected void add(List<I> list) {
        DynamoDbTable<I> table = getTable();
        list.stream().forEach(table::updateItem);
    }

    protected void save(final I item) {
        DynamoDbTable<I> orderTable = getTable();
        orderTable.putItem(item);
    }

    private DynamoDbTable<I> getTable() {
        return dynamoDbenhancedClient.table(getTableName(),
                TableSchema.fromBean(
                        (Class<I>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]
                ));
    }

    protected List<I> scan() {
        DynamoDbTable<I> table = getTable();
        return table.scan().items().stream().collect(Collectors.toList());
    }

    protected <T, V> I getItemByPartition(final T pk, final Optional<V> sk) {
        DynamoDbTable<I> table = getTable();

        Key.Builder key = Key.builder().partitionValue((AttributeValue) pk);
        if (sk.isPresent()) {
            //key.sortValue((AttributeValue) sk.get());
            key.sortValue((String) sk.get());
        }
        return table.getItem(GetItemEnhancedRequest.builder()
                .consistentRead(getConsistentRead())
                .key(key.build())
                .build());
    }

    protected <T> List<I> findBySecundaryKey(final T pk, final String indexName) {
        List<I> resultList = new ArrayList<>();
        DynamoDbIndex<I> secIndex = getTable().index(indexName);

        QueryConditional queryConditional = QueryConditional
                .keyEqualTo(Key.builder().partitionValue((AttributeValue) pk).build());

        Iterable<Page<I>> results = secIndex.query(QueryEnhancedRequest.builder()
                .queryConditional(queryConditional)
                //.consistentRead(getConsistentRead())
                .build());

        results.forEach(y -> y.items().stream().forEach(resultList::add));

        return resultList;
    }

    public String getTableName() {
        return tableName;
    }

    public Boolean getConsistentRead() {
        return consistentRead;
    }
}

