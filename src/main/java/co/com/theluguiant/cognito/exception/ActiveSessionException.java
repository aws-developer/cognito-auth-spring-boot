package co.com.theluguiant.cognito.exception;

public class ActiveSessionException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public ActiveSessionException(String message) {
        super(message);
    }
}
