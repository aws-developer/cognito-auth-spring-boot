package co.com.theluguiant.cognito.exception;

public class NewPasswordRequiered extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NewPasswordRequiered() {
        super("New Password is required");
    }
}