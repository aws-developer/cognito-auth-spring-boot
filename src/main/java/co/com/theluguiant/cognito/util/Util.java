package co.com.theluguiant.cognito.util;

import co.com.theluguiant.cognito.domain.response.Response;
import org.springframework.http.HttpStatus;

public class Util {
    private Util() {
    }

    public static <T> Response<T> callResponse(String msg, Object payload) {
        Response<T> response = new Response<T>();
        response.setData((T) payload);
        response.setMessage(msg);
        response.setStatus(HttpStatus.OK.value());
        return response;
    }

}