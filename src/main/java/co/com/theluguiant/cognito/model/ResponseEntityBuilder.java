package co.com.theluguiant.cognito.model;

import co.com.theluguiant.cognito.domain.response.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseEntityBuilder {
    public static ResponseEntity<Object> build(Response<Object> err) {
        return new ResponseEntity<Object>(err, HttpStatus.valueOf(err.getStatus()));
    }
}

