package co.com.theluguiant.cognito.model.dynamodb;

import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbAttribute;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSortKey;

@DynamoDbBean
public class UserSession {
    private String ClientId;
    private String UserName;
    private String expdate;
    private String Status;

    @DynamoDbPartitionKey
    @DynamoDbAttribute("ClientId")
    public String getClientId() {
        return ClientId;
    }

    public void setClientId(String clientId) {
        ClientId = clientId;
    }

    @DynamoDbSortKey
    @DynamoDbAttribute("UserName")
    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    @DynamoDbAttribute("expdate")
    public String getExpdate() {
        return expdate;
    }

    public void setExpdate(String expdate) {
        this.expdate = expdate;
    }

    @DynamoDbAttribute("Status")
    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    @Override
    public String toString() {
        return "UserSession{" +
                "ClientId='" + ClientId + '\'' +
                ", UserName='" + UserName + '\'' +
                ", expdate='" + expdate + '\'' +
                ", Status='" + Status + '\'' +
                '}';
    }
}
