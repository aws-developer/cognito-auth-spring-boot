package co.com.theluguiant.cognito.rest;

import co.com.theluguiant.cognito.business.CognitoBusiness;
import co.com.theluguiant.cognito.domain.request.UserSignInRequest;
import co.com.theluguiant.cognito.domain.request.UserSignUpRequest;
import co.com.theluguiant.cognito.domain.response.Response;
import co.com.theluguiant.cognito.domain.response.SignInResponse;
import com.amazonaws.services.cognitoidp.model.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api-auth")
public class CoreRest {

    private CognitoBusiness cognitoBusiness;

    @Autowired
    public void setCognitoBusiness(CognitoBusiness cognitoBusiness) {
        this.cognitoBusiness = cognitoBusiness;
    }

    @PostMapping(value = "/sign-up", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<UserType> signUp(UserSignUpRequest request)  throws Exception{
        return cognitoBusiness.signUp(request);
    }

    @PostMapping(value = "/sign-in", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<SignInResponse> signIn(UserSignInRequest request)  throws Exception{
        return cognitoBusiness.signIn(request);
    }
}
