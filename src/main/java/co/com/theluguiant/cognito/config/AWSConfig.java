package co.com.theluguiant.cognito.config;

import com.amazonaws.auth.*;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

@Configuration
@EnableScheduling
public class AWSConfig {
    @Value("${amazon.aws.accesskey}")
    private String accessKey;

    @Value("${amazon.aws.secretkey}")
    private String secretKey;

    @Value("${amazon.aws.region}")
    private String region;

    @Bean
    public AwsCredentialsProvider getAwsCredentials(){
        AwsCredentialsProvider credentialsProvider;
        if (accessKey != null && !accessKey.isEmpty()) {
            AwsBasicCredentials awsCreds = AwsBasicCredentials.create(accessKey, secretKey);
            credentialsProvider = StaticCredentialsProvider.create(awsCreds);
        } else {
            credentialsProvider = DefaultCredentialsProvider.builder().build();
        }
        return credentialsProvider;
    }

    @Bean
    public AWSCredentials getAWSCredentials(){
        AWSCredentials credentials;
        if (accessKey != null && !accessKey.isEmpty()) {
            BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
            credentials = new AWSStaticCredentialsProvider(awsCreds).getCredentials();
        } else {
            credentials = new DefaultAWSCredentialsProviderChain().getCredentials();
        }
        return credentials;
    }

    @Bean
    public DynamoDbClient getDynamoDbClient() {
        return DynamoDbClient.builder().region(Region.of(region))
                .credentialsProvider(getAwsCredentials()).build();
    }

    @Bean
    public DynamoDbEnhancedClient getDynamoDbEnhancedClient() {
        return DynamoDbEnhancedClient.builder()
                .dynamoDbClient(getDynamoDbClient())
                .build();
    }

    @Bean
    public AWSCognitoIdentityProvider cognitoClient() {
        return AWSCognitoIdentityProviderClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(getAWSCredentials()))
                .withRegion(region)
                .build();
    }

}