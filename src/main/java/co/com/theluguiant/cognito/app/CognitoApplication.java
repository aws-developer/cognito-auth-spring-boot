package co.com.theluguiant.cognito.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackages = {"co.com.theluguiant.cognito"})
public class CognitoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CognitoApplication.class, args);
	}

}
