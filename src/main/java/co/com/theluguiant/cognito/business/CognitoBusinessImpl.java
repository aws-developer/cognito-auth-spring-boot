package co.com.theluguiant.cognito.business;

import co.com.theluguiant.cognito.domain.request.UserSignInRequest;
import co.com.theluguiant.cognito.domain.request.UserSignUpRequest;
import co.com.theluguiant.cognito.domain.response.Response;
import co.com.theluguiant.cognito.domain.response.SignInResponse;
import co.com.theluguiant.cognito.exception.ActiveSessionException;
import co.com.theluguiant.cognito.exception.NewPasswordRequiered;
import co.com.theluguiant.cognito.model.dynamodb.UserSession;
import co.com.theluguiant.cognito.repository.UserSessionRepository;
import co.com.theluguiant.cognito.util.Util;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.*;
import org.passay.CharacterData;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Value;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static co.com.theluguiant.cognito.util.Messages.GET_DATA_OK;

@Service
public class CognitoBusinessImpl implements CognitoBusiness {

    final static Logger logger = LoggerFactory.getLogger(CognitoBusinessImpl.class);

    @Value("#{new Boolean('${amazon.cognito.emailVerified}')}")
    private Boolean emailVerified;

    @Value("#{new Boolean('${amazon.cognito.phoneNumberVerified}')}")
    private Boolean phoneNumberVerified;

    @Value("#{new Boolean('${amazon.dynamodb.usersession.activeValidation}')}")
    private Boolean activeValidation;

    @Value("${amazon.cognito.clientId}")
    private String clientId;

    @Value("${amazon.cognito.userPoolId}")
    private String userPoolId;

    @Value("${amazon.cognito.endpoint}")
    private String endpoint;

    @Value("${amazon.cognito.identityPoolId}")
    private String identityPoolId;

    private AWSCognitoIdentityProvider cognitoClient;

    private UserSessionRepository userSessionRepository;

    @Autowired
    public void setCognitoClient(AWSCognitoIdentityProvider cognitoClient) {
        this.cognitoClient = cognitoClient;
    }

    @Autowired
    public void setUserSessionRepository(UserSessionRepository userSessionRepository) {
        this.userSessionRepository = userSessionRepository;
    }

    @Override
    public Response<UserType> signUp(UserSignUpRequest signUpRequest) throws Exception {

        String tempPwd = generateValidPassword();
        AdminCreateUserRequest cognitoRequest = new AdminCreateUserRequest()
                .withUserPoolId(userPoolId)
                .withUsername(signUpRequest.getUsername())
                .withUserAttributes(
                        new AttributeType()
                                .withName("email")
                                .withValue(signUpRequest.getEmail()),
                        new AttributeType()
                                .withName("name")
                                .withValue(signUpRequest.getName()),
                        new AttributeType()
                                .withName("family_name")
                                .withValue(signUpRequest.getFamilyName()),
                        new AttributeType()
                                .withName("last_name")
                                .withValue(signUpRequest.getLastName()),
                        new AttributeType()
                                .withName("phone_number")
                                .withValue(signUpRequest.getPhoneNumber()),
                        new AttributeType()
                                .withName("custom:nationality")
                                .withValue(signUpRequest.getNationality()),
                        new AttributeType()
                                .withName("custom:companyName")
                                .withValue(signUpRequest.getCompanyName()),
                        new AttributeType()
                                .withName("phone_number_verified")
                                .withValue(phoneNumberVerified.toString()),
                        new AttributeType()
                                .withName("email_verified")
                                .withValue(emailVerified.toString()))
                .withTemporaryPassword(tempPwd)
                .withMessageAction("SUPPRESS")
                .withDesiredDeliveryMediums(DeliveryMediumType.EMAIL)
                .withForceAliasCreation(false);

        AdminCreateUserResult createUserResult = cognitoClient.adminCreateUser(cognitoRequest);

        UserType cognitoUser = createUserResult.getUser();

        signUpRequest.getRoles().forEach(r -> addUserToGroup(signUpRequest.getEmail(), r));

        cognitoClient.shutdown();
        return Util.callResponse(GET_DATA_OK, cognitoUser);
    }

    @Override
    public Response<SignInResponse> signIn(UserSignInRequest request) throws Exception {

        if(activeValidation){
            Optional user = Optional.of(request.getUsername());
            UserSession userSession = userSessionRepository.findByClientIDAndUser(clientId, user);

            if(userSession != null){
                logger.debug("Existe una session activa");
                throw new ActiveSessionException("Ya existe una session activa");
            }
        }

        SignInResponse responseData = new SignInResponse();

        final Map<String, String> authParams = new HashMap<>();
        authParams.put("USERNAME", request.getUsername());
        authParams.put("PASSWORD", request.getPassword());

        final AdminInitiateAuthRequest authRequest = getAuthRequest(authParams);

        AdminInitiateAuthResult result = cognitoClient.adminInitiateAuth(authRequest);

        AuthenticationResultType authenticationResult = null;

        if (result.getChallengeName() != null && !result.getChallengeName().isEmpty()) {
            logger.debug("Challenge Name is ",result.getChallengeName());
            if (result.getChallengeName().contentEquals("NEW_PASSWORD_REQUIRED")) {

                if (request.getNewPassword() == null) {
                    logger.debug("Se requiere el parametro newpassword");
                    throw new NewPasswordRequiered();
                }

                final Map<String, String> challengeResponses = new HashMap<>();
                challengeResponses.put("USERNAME", request.getUsername());
                challengeResponses.put("PASSWORD", request.getPassword());
                // add new password
                challengeResponses.put("NEW_PASSWORD", request.getNewPassword());

                final AdminRespondToAuthChallengeRequest requestData =
                        new AdminRespondToAuthChallengeRequest()
                                .withChallengeName(ChallengeNameType.NEW_PASSWORD_REQUIRED)
                                .withChallengeResponses(challengeResponses)
                                .withClientId(clientId).withUserPoolId(userPoolId)
                                .withSession(result.getSession());

                AdminRespondToAuthChallengeResult resultChallenge =
                        cognitoClient.adminRespondToAuthChallenge(requestData);
                authenticationResult = resultChallenge.getAuthenticationResult();

                responseData = getSignInResponse(authenticationResult);
            }
        } else {
            authenticationResult = result.getAuthenticationResult();
            responseData = getSignInResponse(authenticationResult);
        }
        cognitoClient.shutdown();
        return Util.callResponse(GET_DATA_OK, responseData);
    }

    public SignInResponse getSignInResponse(AuthenticationResultType authenticationResult){
        SignInResponse responseData = new SignInResponse();

        responseData.setAccessToken(authenticationResult.getAccessToken());
        responseData.setIdToken(authenticationResult.getIdToken());
        responseData.setRefreshToken(authenticationResult.getRefreshToken());
        responseData.setExpiresIn(authenticationResult.getExpiresIn());
        responseData.setTokenType(authenticationResult.getTokenType());

        return responseData;
    }

    public void addUserToGroup(String username, String groupName) {
        AdminAddUserToGroupRequest addUserToGroupRequest = new AdminAddUserToGroupRequest()
                .withGroupName(groupName)
                .withUserPoolId(userPoolId)
                .withUsername(username);

        cognitoClient.adminAddUserToGroup(addUserToGroupRequest);
    }

    protected AdminInitiateAuthRequest getAuthRequest(Map<String, String> authParams){

        AdminInitiateAuthRequest authRequest = new AdminInitiateAuthRequest();
        authRequest.withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                .withClientId(clientId)
                .withUserPoolId(userPoolId)
                .withAuthParameters(authParams);

        return authRequest;
    }

    private String generateValidPassword() {
        logger.debug("Generando password temporal");

        PasswordGenerator gen = new PasswordGenerator();
        CharacterData lowerCaseChars = EnglishCharacterData.LowerCase;
        CharacterRule lowerCaseRule = new CharacterRule(lowerCaseChars);
        lowerCaseRule.setNumberOfCharacters(2);

        CharacterData upperCaseChars = EnglishCharacterData.UpperCase;
        CharacterRule upperCaseRule = new CharacterRule(upperCaseChars);
        upperCaseRule.setNumberOfCharacters(2);

        CharacterData digitChars = EnglishCharacterData.Digit;
        CharacterRule digitRule = new CharacterRule(digitChars);
        digitRule.setNumberOfCharacters(2);

        CharacterData specialChars = new CharacterData() {
            public String getErrorCode() {
                return "ERRONEOUS_SPECIAL_CHARS";
            }

            public String getCharacters() {
                return "!@#$%^&*()_+";
            }
        };
        CharacterRule splCharRule = new CharacterRule(specialChars);
        splCharRule.setNumberOfCharacters(2);

        return gen.generatePassword(10, splCharRule, lowerCaseRule,
                upperCaseRule, digitRule);
    }


}
