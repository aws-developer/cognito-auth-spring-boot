package co.com.theluguiant.cognito.business;

import co.com.theluguiant.cognito.domain.request.UserSignInRequest;
import co.com.theluguiant.cognito.domain.request.UserSignUpRequest;
import co.com.theluguiant.cognito.domain.response.Response;
import co.com.theluguiant.cognito.domain.response.SignInResponse;
import com.amazonaws.services.cognitoidp.model.UserType;

public interface CognitoBusiness {
    Response<UserType> signUp(UserSignUpRequest signUpRequest) throws Exception;
    Response<SignInResponse> signIn(UserSignInRequest request) throws Exception;
}
